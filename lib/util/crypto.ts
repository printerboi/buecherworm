import crypto, {randomBytes} from "crypto";
import {prisma} from "@/app/api/_base";


async function getSessionpwd(){
    const record = await prisma.settings.findFirst({
        where: {
            name: "sessionpwd"
        }
    });

    if (record?.value){
        return record.value;
    }
    return undefined;
}

export const encrypt = async (plaintext: string) => {
    let sessionpwd = await getSessionpwd();
    if(sessionpwd === undefined) {
        throw new Error("SESSIONKEY NOT DEFINED!");
    }

    const iv = crypto.randomBytes(16);
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(sessionpwd), iv);
    let encrypted = cipher.update(plaintext);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv, encryptedData: encrypted.toString('hex') };
}

export const decrypt = async (ciphertext: { iv: string, encryptedData: string }) => {
    let sessionpwd = await getSessionpwd();
    if(sessionpwd === undefined) {
        throw new Error("SESSIONKEY NOT DEFINED!");
    }

    let ivBuffer = Buffer.from(ciphertext.iv, 'hex');
    let encryptedText = Buffer.from(ciphertext.encryptedData, 'hex');
    const decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(sessionpwd), ivBuffer);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}
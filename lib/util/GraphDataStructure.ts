
export interface CanvasNode {
    id: string,
    label: string,
    size: number
    color: string;
}

export interface CanvasEdge {
    id: string,
    source: string,
    target: string,
    label: string
}

class TagNode {
    id: string;
    occurrences: number;

    constructor(id: string) {
        this.id = id;
        this.occurrences = 1;
    }
}

class Edge {
    start: TagNode;
    end: TagNode;

    constructor(startNode: TagNode, endNode: TagNode) {
        this.start = startNode;
        this.end = endNode;
    }
}

export class TagGraph {
    nodes: Array<TagNode>;
    edges: Array<Edge>;

    constructor(){
        this.edges = [];
        this.nodes = [];
    }

    private getNode(tag: string): TagNode | undefined {
        const node = this.nodes.find((n: TagNode) => { return n.id === tag.toLowerCase() });
        return node;
    }

    toCanvasNodes(): Array<CanvasNode> {
        return this.nodes.map((node: TagNode) => {
            const hashnumber = hashCode(node.id);
            const randomColor = Math.floor(hashnumber%16777215).toString(16);
            return { id: node.id, label: node.id, size: node.occurrences, color: `#${randomColor}` };
        });
    }

    toCanvasEdges(): Array<CanvasEdge> {
        return this.edges.map((edge: Edge) => {
            const startId = edge.start.id;
            const endId = edge.end.id;

            return { id: `${startId}->${endId}`, source: startId, target: endId, label: "" };
        });
    }

    insertTag(tag: string) {
        const keysFound = this.nodes.filter((nodeToCheck: TagNode) => {
            return nodeToCheck.id === tag.toLowerCase();
        });
        
        if(keysFound.length === 0){
            this.nodes.push(new TagNode(tag.toLowerCase()));
        }else if(keysFound.length === 1){
            keysFound[0].occurrences = keysFound[0].occurrences + 1;
        }
    }
    
    insertNetwork(tags: Array<string>) {
        // Catch special cases 0 and 1. In these cases no edges must be inserted!
        if(tags.length > 1){
            if(tags.length == 2){
                const start = this.getNode(tags[0]);
                const end = this.getNode(tags[1]);
                if(start !== undefined && end !== undefined){
                    const edge = new Edge(start, end);
                    this.edges.push(edge);
                }
            }else{
                for(let i = 0; i < tags.length-1; i++) {
                    const start = this.getNode(tags[i]);
                    const end = this.getNode(tags[i+1]);
                    if(start !== undefined && end !== undefined){
                        const edge = new Edge(start, end);
                        this.edges.push(edge);
                    }
                }

                const start = this.getNode(tags[tags.length - 1]);
                const end = this.getNode(tags[0]);

                if(start !== undefined && end !== undefined){
                    const edge = new Edge(start, end);
                    this.edges.push(edge);
                }
            }
        }
    }
}


const hashCode = (str: string) => {
    var hash = 0,
        i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}
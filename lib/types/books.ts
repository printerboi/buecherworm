
export interface Publisher{
    id: number,
    name: string,
    books: Array<Book>
}

export interface Author{
    id: number,
    name: string,
    books: Array<Book>
}

export enum BookState {
    NOTREAD,
    INREAD,
    FINISHED
}

export interface Book{
    id: number,
    name: string,
    authors: Array<Author>,
    publisher: Publisher,
    year: number,
    pages: number,
    rating?: number,
    tags: Array<string>,
    comment?: string
    finishedAt?: number,
    state: BookState,
    isbn: string,
    loadImage: boolean
}
import type { NextRequest } from 'next/server'
import {cookies} from "next/headers";
import crypto from "crypto";
import {decrypt} from "@/lib/util/crypto";
import axios from "axios";

export async function middleware(request: NextRequest) {
    const currentUserEncrypted = cookies().get('buecherworm_token')?.value;


    if(currentUserEncrypted !== undefined) {
        try{
            const validRed = await axios.post(`${process.env.BUECHERWORM_HOSTNAME}/api/validateKey`, {
                user: currentUserEncrypted
            });

            console.log("VALIDATION done")
            const currentUser = validRed.data.valid;
            console.log(!currentUser);

            if (currentUser && !request.nextUrl.pathname.startsWith('/backend')) {
                return Response.redirect(new URL('/backend/dashboard', request.url));
            }

            if (!currentUser && !request.nextUrl.pathname.startsWith('/login')) {
                return Response.redirect(new URL('/login', request.url));
            }
        }catch (e){
            console.log(e);
            if(!request.nextUrl.pathname.startsWith('/login')){
                return Response.redirect(new URL('/login', request.url))
            }
        }
    }else{
        if(!request.nextUrl.pathname.startsWith('/login') && !request.nextUrl.pathname.startsWith('/register')){
            return Response.redirect(new URL('/login', request.url))
        }

    }
}

export const config = {
    matcher: ['/backend/:path*', '/login', '/register'],
}
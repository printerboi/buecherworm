import {useEffect, useState} from "react";
import axios from "axios";


export default function Footer() {
    const [legallink, setLegallink] = useState<string>("");
    const [privacylink, setPrivacylink] = useState<string>("");

    useEffect(() => {

        const getLinks = async () => {
            const request = await axios.get("/api/links");
            setLegallink(request.data.legal);
            setPrivacylink(request.data.privacy);
        }

        getLinks()
    }, []);

    const LegalLinkElement = () => {
        if(legallink != ""){
            return (
                <li>
                    <a href={legallink} className="hover:underline mx-16">Legal</a>
                </li>
            );
        }

        return <></>;
    }

    const PrivacyLinkElement = () => {
        if(privacylink != ""){
            return (
                <li>
                    <a href={privacylink} className="hover:underline">Privacy</a>
                </li>
            );
        }

        return <></>;
    }

    return (
        <footer className="bg-white rounded-lg shadow m-4 dark:bg-gray-800 my-16">
            <div className="w-full mx-auto flex flex-col md:flex-row max-w-screen-xl p-4 md:flex md:items-center md:justify-between">
                <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400"><a href="https://gitlab.com/printerboi/buecherworm" className="hover:underline">Buecherworm</a></span>
                <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">Covers provided by <a className="text-indigo-300" href="https://openlibrary.org/">open library</a></span>
                <ul className="flex flex-wrap justify-center items-center mt-3 text-sm font-medium text-gray-500 dark:text-gray-400 sm:mt-0">
                    <PrivacyLinkElement />
                    <LegalLinkElement />
                </ul>
            </div>
        </footer>
    );
}
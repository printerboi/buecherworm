
<p align="center">
<img src="public/logo.png" alt="drawing" width="200"/>
</p>

# Buecherworm

Welcome to Buecherworm, your personal library organizer and book showcase!
This project is designed to help you manage your book collection efficiently and elegantly display it. Whether you're an avid reader or a book collector, Buecherworm offers the perfect solution to keep your library organized and visually appealing

## Features

Buecherworm enables you to manage your personal library by saving your books to a database.
Once saved, the books will appear on the application's frontend. In addition to that buecherworm will
display all kinds of metadata saved by you.

## Setup
Buecherworm **requires** a postgres database.
Please make sure, that the postgres database is running and can be accessed. Buecherworm will run some
scripts during the first startup to initialize the database. 
The setup is based on docker. The Image can be run with:

```
docker run \
    -e BUECHERWORM_PEPPER=<A secure pepper value> \
    -e BUECHERWORM_POSTGRES_USER=<Postgres username> \
    -e BUECHERWORM_POSTGRES_PASSWORD=<Postgres password> \
    -e BUECHERWORM_POSTGRES_HOST=<Postgres host> \
    -e BUECHERWORM_POSTGRES_PORT=<Postgres port> \
    -e BUECHERWORM_LEGAL_LINK=<Link in Footer> \
    -e BUECHERWORM_PRIVACY_LINK=<Link in Footer> \
    -e BUECHERWORM_HOSTNAME=<Hostname of this service> \
    -p 7766:3000 \
    registry.gitlab.com/printerboi/buecherworm:latest
```

Alternatively you can use the following `docker-compose.yml`:

```
services:
  buecherworm:
    image: printerboi/buecherworm
    restart: always
    ports:
      - 7766:3000
    links:
      - "db:database"

  db:
    image: postgres
    restart: always
    # set shared memory limit when using docker-compose
    shm_size: 128mb
    environment:
      POSTGRES_USER: ${BUECHERWORM_POSTGRES_USER}
      POSTGRES_PASSWORD: ${BUECHERWORM_POSTGRES_PASSWORD}
    ports:
      - 5432:5432
    volumes:
      - ./database/init.sql:/docker-entrypoint-initdb.d/init.sql
```
For a list of the needed environment variables see the following section.

## Environment variables
Buecherworm requires some environment variables to function. See the following table:

| Variable                      | Meaning                                                           |
|-------------------------------|-------------------------------------------------------------------|
| BUECHERWORM_PEPPER            | A secure pepper used during password generation on first register |
| BUECHERWORM_POSTGRES_USER     | Username used to access the postgres database                     |
| BUECHERWORM_POSTGRES_PASSWORD | Password used to access the postgres database                     |
| BUECHERWORM_POSTGRES_HOST     | Host the postgres database is running on                          |
| BUECHERWORM_POSTGRES_PORT     | Port the postgres database is running on                          |
| BUECHERWORM_LEGAL_LINK        | A link that will be placed in the frontends footer                |
| BUECHERWORM_PRIVACY_LINK      | Another link that will be placed in the frontends footer          |
| BUECHERWORM_HOSTNAME          | Hostname buecherworm is running on. Needed for usage behind proxy |

## Usage

After the first startup (watch out this might take a while) visit `buecherworm-hostname.xyz/login`.
You will be prompted to register a user account.

![](doc/readme/register.png)

After the initial registration you will be redirected to the backend:

![](doc/readme/backend.png)

Here you can manage your books and view interesting stats about them.
Notice the node graph here especially. Each book can be described by a list of tags. These tags will be displayed in the 
graph and show the relation between the tags. The size of each node represents the number of books the tag is used in.
The links between nodes occur, if two tags were used in the same book. Hover over the nodes to see their name.

In the frontend you will get a presentation of your read books.
Under the root of your hostname, all books that were read this year will be display:
![](doc/readme/read.png)

Each container is clickable and will reveal a detailed view about it:
![](doc/readme/detail.png)


## Known Issues

### Thumbnails
 - Currently, the thumbnails of the books will be fetched from [OpenLibrary](https://openlibrary.org/) (Go check out their website if you didn't know about them.)
Therefore, the image loading is slow and does not work at times.
 - The loading of the images is dependent on the ISBN of the book. Some books do not have covers at [OpenLibrary](https://openlibrary.org/). You can view all the covers
on their page and choose a different one with a different ISBN if your cover is missing
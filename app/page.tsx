"use client";
import {useEffect, useState} from "react";
import {Book} from "@/lib/types/books";
import HeaderBar, {HeadBarMenuItem} from "@/components/HeadBar/HeadBar";
import BookPresentation from "@/components/BookPresentation/BookPresentation";
import {databases} from "@/appwrite";
import {Query} from "appwrite";
import {number} from "prop-types";
import axios from "axios";
import Footer from "@/components/Footer/Footer";

export default function Home() {
    const [ books, setBooks ] = useState(Array<Book>());
    const [ loading, setLoading ] = useState(true);

    useEffect(() => {
        const getData = async () => {
            const result = await axios.get("/api/book?state=2");

            if(result.data.books){
                setBooks(( result.data.books as unknown ) as Array<Book>);
                setLoading(false);
            }
        }

        getData();
    }, []);

    const BookGrid = () => {
        const GRIDELEMENTS = 4;
        const rowNumber = Math.ceil(books.length / 4 );
        const rows = [];

        for(let row = 0; row < rowNumber; row++){
            //rows.push(<div key={`barrier-${row}-0`} className="col-span-1"></div>);
            for(let book = 0; book < GRIDELEMENTS; book++){
                const index = row*GRIDELEMENTS + book;
                if(index >= books.length){
                    rows.push(<div key={index} className="col-span-1"></div>);
                }else{
                    const bookObject = books[index];
                    rows.push(<BookPresentation key={index} book={bookObject}/>)
                }
            }
            //rows.push(<div key={`barrier-${row}-1`} className="col-span-1"></div>);
        }


        return rows;
    }

    const BookDisplay = () => {
        if(!loading){
            if(books.length != 0){
                return(
                    <div className="grid place-items-center md:grid-cols-3 lg:grid-cols-4 gap-8 sm:grid-cols-1">
                        <BookGrid />
                    </div>
                );
            } else {
                return (
                    <div className="mt-32 w-full flex flex-row justify-center">
                        <div className="col-span-2"/>
                        <div
                            className="col-span-2 px-3 flex flex-row justify-center items-center font-bold text-4xl text-center text-gray-400">
                            <h2>I did not read any books this year...</h2>
                        </div>
                        <div className="col-span-2"/>
                    </div>
                );
            }
        } else {
            return (
                <div role="status" className="mt-32 w-full flex flex-row justify-center">
                    <svg aria-hidden="true"
                         className="w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
                         viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                            fill="currentColor"/>
                        <path
                            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                            fill="currentFill"/>
                    </svg>
                    <span className="sr-only">Loading...</span>
                </div>

            );
        }
    }

    return (
        <div className="py-16">
            <div className="container mx-auto h-screen flex flex-col justify-between">
                <HeaderBar active={HeadBarMenuItem.HOME}/>
                <div className="w-full ">
                    <BookDisplay/>
                </div>
                <Footer/>
            </div>
        </div>
    );
}

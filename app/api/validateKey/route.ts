
import {cookies} from "next/headers";
import {decrypt} from "@/lib/util/crypto";


interface LoginData {
    username: string;
    password: string;
}

export async function POST(req: Request) {

    try{
        const { user } = await req.json();
        const baseDecoded = JSON.parse(atob(user));
        const currentUser = await decrypt(baseDecoded);

        return Response.json({
            errorCode: 0,
            valid: true
        }, { status: 200 });

    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            valid: false
        }, { status: 500 });
    }
}
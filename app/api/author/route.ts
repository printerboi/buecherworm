import {NextApiRequest, NextApiResponse} from "next";
import { PrismaClient } from '@prisma/client'
import type { Author } from "@prisma/client"
import {prisma} from "@/app/api/_base";

interface AuthorCreationResponse {
    errorCode: number,
    msg?: string
    author: Author;
}

interface AuthorCreationData {
    name: string;
}

export async function POST(req: Request) {
    try{
        const data: AuthorCreationData = await req.json() as AuthorCreationData;

        console.log(data);

        try{
            const createdAuthor = await prisma.author.create({
                data: {
                    name: data.name
                }
            });

            console.log(createdAuthor);

            return Response.json({
                errorCode: 0,
                author: createdAuthor,
            }, { status: 200 });
        }catch (e){
            console.error(e);
            return Response.json({
                errorCode: -2,
                message: "Creation of the object resulted in error",
            }, { status: 500 });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation",
        }, { status: 400 });
    }
}


export async function GET(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        const includeBooks = searchParams.get('withBooks');

        if(!idParam){
            try {
                let includeBooksFlag= false;
                if(includeBooks){
                    includeBooksFlag = parseInt(includeBooks) == 1;
                }

                let foundAuthors = [];

                if(includeBooksFlag){
                    foundAuthors = await prisma.author.findMany({
                        include: {
                            books: true
                        }
                    })
                }else{
                    foundAuthors = await prisma.author.findMany()
                }

                return Response.json({
                    errorCode: 0,
                    authors: foundAuthors,
                })

            }catch (e){
                console.error(e);
                return Response.json({
                    errorCode: -2,
                    message: "Query resulted in an error",
                });
            }
        }else{
            const id = parseInt(idParam);

            try {
                const foundAuthors = await prisma.author.findFirst({
                    where: {
                        id: id
                    }
                });

                return Response.json({
                    errorCode: 0,
                    author: foundAuthors,
                })

            }catch (e){
                console.error(e);
                return Response.json({
                    errorCode: -2,
                    message: "Query resulted in an error",
                });
            }
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation"
        });
    }
}

export async function PUT(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        if(!idParam) return Response.json({
            errorCode: -3,
            message: "Please provide an id as natural number"
        })
        const id = parseInt(idParam);
        const data: AuthorCreationData = await req.json() as AuthorCreationData;

        try{
            const createdAuthor = await prisma.author.update({
                data: {
                    name: data.name
                },
                where: {
                    id: id
                }
            });

            console.log(createdAuthor);

            return Response.json({
                errorCode: 0,
                author: createdAuthor,
            }, { status: 200 });
        }catch (e){
            console.error(e);
            return Response.json({
                errorCode: -2,
                message: "Creation of the object resulted in error",
            }, { status: 500 });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation",
        }, { status: 400 });
    }
}

export async function DELETE(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        if(!idParam) return Response.json({
            errorCode: -3,
            message: "Please provide an id as natural number"
        })
        const id = parseInt(idParam);

        try {
            await prisma.author.delete({
                where: {
                    id: id
                }
            });

            return Response.json({
                errorCode: 0,
            })

        }catch (e){
            console.error(e);
            return Response.json({
                errorCode: -2,
                message: "Deletion resulted in an error",
            });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation"
        });
    }
}
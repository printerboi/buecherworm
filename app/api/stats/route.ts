import {Book, PrismaClient} from "@prisma/client";
import {BookState} from "@/lib/types/books";
import moment from "moment";
import {TagGraph} from "@/lib/util/GraphDataStructure";
import {prisma} from "@/app/api/_base";
export const dynamic = 'force-dynamic'

export async function GET(req: Request) {
    let allBooks: Array<Book> = await prisma.book.findMany({});
    let unreadBooks = allBooks.filter((book: Book) => {
        return book.state === 0 || book.state == 1;
    });

    const thisYear = new Date().getFullYear();
    const booksThisYear = allBooks.filter((book: Book) => {
       const bookYear = moment(book.finishedAt).year();
       return bookYear == thisYear;
    });

    /*const nodes: Array<{ id: string, label: string }> = [];
    const edges: Array<{ id: string, label: string, source: string, target: string }> = [];*/

    const graph = new TagGraph();

    allBooks.forEach((book: Book) => {
       const tags = book.tags;
       tags.forEach((tag: string) => {
           graph.insertTag(tag);
       })

        graph.insertNetwork(tags);
    });

    const nodes = graph.toCanvasNodes();
    const edges = graph.toCanvasEdges();

    return Response.json({
        errorCode: 0,
        stats: {
            bookCount: allBooks.length,
            unreadBooks: unreadBooks.length,
            booksPearMonthThisYear: booksThisYear.length/12,
            tagNetwork: {
                nodes: nodes,
                edges: edges,
            }
        },
    })
}

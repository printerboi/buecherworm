import {PrismaClient} from "@prisma/client";
import bcrypt from "bcrypt";
import {cookies} from "next/headers";
import {randomUUID} from "node:crypto";
import {prisma} from "@/app/api/_base";
import {decrypt, encrypt} from "@/lib/util/crypto";
import {btoa} from "node:buffer";

interface LoginData {
    username: string;
    password: string;
}


export async function POST(req: Request) {
    const data: LoginData = await req.json() as LoginData;

    try{
        const requestData = {
            username: data.username,
            password: data.password
        }

        const userRecord = await prisma.user.findFirst({
            where: {
                username: requestData.username,
            }
        });

        if(userRecord){
            const passwordHash = userRecord.password;
            const insertedPassword = requestData.password + userRecord.salt + process.env.BUECHERWORM_PEPPER;
            console.log(insertedPassword);
            console.log(passwordHash);
            const compare = bcrypt.compareSync(insertedPassword, passwordHash);

            if(compare){

                const object = await encrypt(randomUUID());
                const baseEncoded = btoa(JSON.stringify(object));

                cookies().set('buecherworm_token', baseEncoded);


                return Response.json({
                    errorCode: 0,
                }, { status: 200 });

            }else{
                console.log("Wrong password");
                return Response.json({
                    errorCode: -1,
                }, { status: 403 });
            }
        }else{
            console.log("No user found!");
            return Response.json({
                errorCode: -1,
            }, { status: 403 });
        }

    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
        }, { status: 403 });
    }
}
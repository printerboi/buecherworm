import {NextApiRequest, NextApiResponse} from "next";
import { PrismaClient } from '@prisma/client'
import type { Book } from "@prisma/client"
import {BookState} from "@/lib/types/books";
import {prisma} from "@/app/api/_base";

const PAGESIZE = 25;

interface BookCreationResponse {
    errorCode: number,
    book: Book;
}

interface BookCreationData {
    name: string;
    state: number;
    pages: number;
    isbn: string;
    comment?: string;
    year: number;
    rating?: number;
    finishedAt: string;
    tags: Array<string>;
    authors: Array<number>;
    publisher: number;
}

export async function POST(req: Request) {

    try{
        const data: BookCreationData = await req.json() as BookCreationData;

        try{
            const createdBook = await prisma.book.create({
                data: {
                    name: data.name,
                    state: data.state,
                    pages: data.pages,
                    isbn: data.isbn,
                    comment: data.comment,
                    year: data.year,
                    rating: data.rating,
                    finishedAt: data.finishedAt,
                    tags: data.tags,
                    authors: {
                        connect: data.authors.map((authorId) => { return { id: authorId } })
                    },
                    publisher: {
                        connect: {
                            id: data.publisher
                        }
                    }
                }
            });

            console.log(createdBook);

            
            return Response.json({
                errorCode: 0,
                book: createdBook,
            }, { status: 200 });
        }catch (e){
            console.error(e);
            
            return Response.json({
                errorCode: -2,
            }, { status: 500 });
        }
    }catch (e){
        console.error(e);
        

        return Response.json({
            errorCode: -1,
        }, { status: 400 });
    }
}

export async function GET(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        const state = searchParams.get('state')

        if(!idParam){
            let search = "";
            let page: number = 0;

            const searchString = searchParams.get('search');
            const pageString = searchParams.get('page');

            if(searchString){ search = searchString };
            if(pageString){ page = parseInt(pageString) };

            try {
                let foundBooks = await prisma.book.findMany({
                    include: {
                        authors: true,
                        publisher: true
                    },
                    where: {
                        OR: [
                            {
                                name: {
                                    contains: search
                                }
                            },
                            {
                                isbn: {
                                    contains: search
                                }
                            }
                        ]
                    },
                    take: PAGESIZE,
                    skip: page * PAGESIZE
                });

                if(state){
                    const stateNumber = parseInt(state);
                    switch (stateNumber){
                        case 0:
                            foundBooks = foundBooks.filter((book: Book) => book.state === BookState.NOTREAD)
                            break;
                        case 1:
                            foundBooks = foundBooks.filter((book: Book) => book.state === BookState.INREAD)
                            break;
                        case 2:
                            foundBooks = foundBooks.filter((book: Book) => book.state === BookState.FINISHED)
                            break;
                    }
                }

                
                return Response.json({
                    errorCode: 0,
                    books: foundBooks,
                })

            }catch (e){
                console.error(e);
                
                return Response.json({
                    errorCode: -2,
                    message: "Query resulted in an error",
                });
            }
        }else{
            const id = parseInt(idParam);

            try {
                const foundBooks = await prisma.book.findFirst({
                    include: {
                        authors: true,
                        publisher: true
                    },
                    where: {
                        id: id
                    }
                });

                
                return Response.json({
                    errorCode: 0,
                    book: foundBooks,
                })

            }catch (e){
                console.error(e);
                
                return Response.json({
                    errorCode: -2,
                    message: "Query resulted in an error",
                });
            }
        }
    }catch (e){
        console.error(e);
        
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation"
        });
    }
}

export async function PUT(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        if(!idParam) {
            
            return Response.json({
                errorCode: -3,
                message: "Please provide an id as natural number"
            })
        }
        const id = parseInt(idParam);
        const data: BookCreationData = await req.json() as BookCreationData;

        console.log(data.authors?.map((authorId) => { return { id: authorId } }));

        try{
            const updatedBook = await prisma.book.update({
                data: {
                    name: data.name,
                    state: data.state,
                    pages: data.pages,
                    isbn: data.isbn,
                    comment: data.comment,
                    year: data.year,
                    rating: data.rating,
                    finishedAt: data.finishedAt,
                    tags: data.tags,
                    authors: {
                        set: data.authors?.map((authorId) => { return { id: authorId } })
                    },
                    publisher: {
                        connect: {
                            id: data.publisher
                        }
                    }
                },
                where: {
                    id: id
                }
            });

            
            return Response.json({
                errorCode: 0,
                book: updatedBook,
            }, { status: 200 });
        }catch (e){
            console.error(e);
            
            return Response.json({
                errorCode: -2,
                message: "Creation of the object resulted in error",
            }, { status: 500 });
        }
    }catch (e){
        console.error(e);
        
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation",
        }, { status: 400 });
    }
}

export async function DELETE(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        if(!idParam) {
            
            return Response.json({
                errorCode: -3,
                message: "Please provide an id as natural number"
            })
        }
        const id = parseInt(idParam);

        try {
            await prisma.book.delete({
                where: {
                    id: id
                }
            });

            
            return Response.json({
                errorCode: 0,
            })

        }catch (e){
            console.error(e);
            
            return Response.json({
                errorCode: -2,
                message: "Deletion resulted in an error",
            });
        }
    }catch (e){
        console.error(e);
        
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation"
        });
    }
}

import {decrypt} from "@/lib/util/crypto";
import * as fs from "fs";
import {prisma} from "@/app/api/_base";



export async function GET(req: Request) {

    try{

        const established = await prisma.settings.findFirst({
            where: {
                name: 'established'
            }
        });

        if(established){
            return Response.json({
                errorCode: 0,
                established: established.value === "1"
            }, { status: 200 });
        }else{
            return Response.json({
                errorCode: 0,
                established: false
            }, { status: 200 });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            valid: false
        }, { status: 500 });
    }
}
import {PrismaClient} from "@prisma/client";
import bcrypt from "bcrypt";
import {cookies} from "next/headers";
import {randomUUID} from "node:crypto";
import {prisma} from "@/app/api/_base";
import {decrypt, encrypt} from "@/lib/util/crypto";
import {btoa} from "node:buffer";
import * as fs from "fs";

interface LoginData {
    username: string;
    password: string;
}


export async function POST(req: Request) {
    const data: LoginData = await req.json() as LoginData;

    try{
        const requestData = {
            username: data.username,
            password: data.password
        }

        const isEstablished = await prisma.settings.findFirst({
            where: {
                name: "established",
            }
        });

        if(!isEstablished){
            const salt = await bcrypt.genSalt();
            const insertedPassword = requestData.password + salt + process.env.BUECHERWORM_PEPPER;

            const hash = bcrypt.hashSync(insertedPassword, 12);
            console.log(hash);

            await prisma.user.create({
                data: {
                    username: data.username,
                    password: hash,
                    salt: salt
                }
            });

            await prisma.settings.create({
                data: {
                    name: "established",
                    value: "1"
                }
            })

            const object = await encrypt(randomUUID());
            const baseEncoded = btoa(JSON.stringify(object));

            cookies().set('buecherworm_token', baseEncoded);


            return Response.json({
                errorCode: 0,
            }, { status: 200 });

        }else{
            console.log("Username already in Use");
            return Response.json({
                errorCode: -1,
            }, { status: 403 });
        }

    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
        }, { status: 403 });
    }
}
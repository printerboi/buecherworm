import {NextApiRequest, NextApiResponse} from "next";
import { PrismaClient } from '@prisma/client'
import type { Publisher } from "@prisma/client"
import {prisma} from "@/app/api/_base";

interface PublisherCreationResponse {
    errorCode: number,
    publisher: Publisher;
}

interface PublisherCreationData {
    name: string;
}

export async function POST(req: Request) {

    try{
        const data: PublisherCreationData = await req.json() as PublisherCreationData;

        try{
            const createdPublisher = await prisma.publisher.create({
                data: {
                    name: data.name
                }
            });

            console.log(createdPublisher);

            return Response.json({
                errorCode: 0,
                publisher: createdPublisher,
            }, { status: 200 });
        }catch (e){
            console.error(e);
            return Response.json({
                errorCode: -2,
            }, { status: 500 });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
        }, { status: 400 });
    }
}

export async function GET(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        const includeBooks = searchParams.get('withBooks');


        if(!idParam){
            try {
                let includeBooksFlag= false;
                if(includeBooks){
                    includeBooksFlag = parseInt(includeBooks) == 1;
                }

                let foundPublishers = [];

                if(includeBooksFlag){
                    foundPublishers = await prisma.publisher.findMany({
                        include: {
                            books: true
                        }
                    })
                }else{
                    foundPublishers = await prisma.publisher.findMany()
                }

                return Response.json({
                    errorCode: 0,
                    publishers: foundPublishers,
                })

            }catch (e){
                console.error(e);
                return Response.json({
                    errorCode: -2,
                    message: "Query resulted in an error",
                });
            }
        }else{
            const id = parseInt(idParam);

            try {
                const foundPublishers = await prisma.publisher.findFirst({
                    where: {
                        id: id
                    }
                });

                return Response.json({
                    errorCode: 0,
                    publisher: foundPublishers,
                })

            }catch (e){
                console.error(e);
                return Response.json({
                    errorCode: -2,
                    message: "Query resulted in an error",
                });
            }
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation"
        });
    }
}

export async function PUT(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        if(!idParam) return Response.json({
            errorCode: -3,
            message: "Please provide an id as natural number"
        })
        const id = parseInt(idParam);
        const data: PublisherCreationData = await req.json() as PublisherCreationData;

        try{
            const updatedPublisher = await prisma.publisher.update({
                data: {
                    name: data.name
                },
                where: {
                    id: id
                }
            });

            return Response.json({
                errorCode: 0,
                publisher: updatedPublisher,
            }, { status: 200 });
        }catch (e){
            console.error(e);
            return Response.json({
                errorCode: -2,
                message: "Creation of the object resulted in error",
            }, { status: 500 });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation",
        }, { status: 400 });
    }
}

export async function DELETE(req: Request) {
    try{
        const { searchParams } = new URL(req.url);
        const idParam = searchParams.get('id');
        if(!idParam) return Response.json({
            errorCode: -3,
            message: "Please provide an id as natural number"
        })
        const id = parseInt(idParam);

        try {
            await prisma.publisher.delete({
                where: {
                    id: id
                }
            });

            return Response.json({
                errorCode: 0,
            })

        }catch (e){
            console.error(e);
            return Response.json({
                errorCode: -2,
                message: "Deletion resulted in an error",
            });
        }
    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            message: "Data missing please relate to the documentation"
        });
    }
}
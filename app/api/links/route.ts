
export async function GET(req: Request) {
    try{
        const legal = process.env.BUECHERWORM_LEGAL_LINK;
        const privacy = process.env.BUECHERWORM_PRIVACY_LINK;

        return Response.json({
            errorCode: 0,
            legal: (legal !== undefined)? legal : "",
            privacy: (privacy !== undefined)? privacy : "",
        }, { status: 200 });

    }catch (e){
        console.error(e);
        return Response.json({
            errorCode: -1,
            legal: "",
            privacy: "",
        }, { status: 500 });
    }
}
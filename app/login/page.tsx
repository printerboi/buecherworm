"use client";
import {FormEvent, useEffect, useState} from "react";
import {useRouter} from "next/navigation";
import Alert from "@/components/Alert/Alert";
import useAuth from "@/components/AuthedContext/useAuth";
import axios from "axios";
import HeaderBar, {HeadBarMenuItem} from "@/components/HeadBar/HeadBar";


export default function Login(){
    const router = useRouter()
    const [username, setUsername] = useState("");
    const [password,setPassword] = useState("");
    const [loading,setLoading] = useState(false);
    const [ error, setError ] = useState(false);
    const {setAuthStatus} = useAuth();

    useEffect(() => {
        const getData = async () => {
            const established = await axios.get("/api/established");
            console.log(established.data?.established)
            if(!(established.data?.established)){
                router.replace("/register");
            }
        }

        getData();
    }, []);

    const signUpWithEmail = async (e:  FormEvent<HTMLFormElement>) => {
        setLoading(true)

        try{
            console.log("trying login...");
            const session = await axios.post("/api/login", {
                username: username,
                password: password
            })

            if (session) {
                setAuthStatus(true);
                console.log("login successfull");
                router.push("/backend/dashboard");
            }
        }catch(err) {
            setLoading(false);
            setError(true);
            console.log(err)
        }
    }

    return(
        <div>
            <HeaderBar active={HeadBarMenuItem.LOGIN} />
            <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
                <div className="sm:mx-auto sm:w-full sm:max-w-sm">
                    <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                        Login
                    </h2>

                    <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
                        <form className="space-y-6" action="#" onSubmit={(e) => {
                            e.preventDefault();
                            signUpWithEmail(e)
                        }}>
                            <div>
                                <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                                    Username
                                </label>
                                <div className="mt-2">
                                    <input
                                        id="username"
                                        name="username"
                                        type="text"
                                        autoComplete="username"
                                        required
                                        onChange={(e) => {
                                            setError(false);
                                            setUsername(e.target.value);
                                        }}
                                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                    />
                                </div>
                            </div>

                            <div>
                                <div className="flex items-center justify-between">
                                    <label htmlFor="password"
                                           className="block text-sm font-medium leading-6 text-gray-900">
                                        Password
                                    </label>
                                </div>
                                <div className="mt-2">
                                    <input
                                        id="password"
                                        name="password"
                                        type="password"
                                        autoComplete="current-password"
                                        required
                                        onChange={(e) => {
                                            setError(false);
                                            setPassword(e.target.value);
                                        }}
                                        className="block w-full rounded-md border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                    />
                                </div>
                            </div>

                            <div>
                                <button
                                    type="submit"
                                    className="flex w-full justify-center rounded-md bg-black px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-gray-900 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Sign in
                                </button>
                            </div>

                            <Alert active={error} text={"Something went wrong. Try again!"}/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}
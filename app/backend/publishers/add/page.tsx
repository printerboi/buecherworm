"use client";
import Sidebar from "@/components/Sidebar/Sidebar";
import {Dispatch, useEffect, useRef, useState} from "react";
import {Author, Book, BookState, Publisher} from "@/lib/types/books";
import {databases, storage} from "@/appwrite";
import {ID, Query} from "appwrite";
import moment from "moment";
import {useRouter} from "next/navigation";
import axios from "axios";



export default function PublisherAdd() {
    const router = useRouter();

    const [ name, setName ] = useState("");



    return (
        <Sidebar page={"publishers"}>
            <div className="flex flex-col items-center">
                <div className="relative overflow-x-auto flex flex-col w-1/2 justify-center my-16">
                    <form className="flex flex-col gap-8" onSubmit={async (formev) => {
                        formev.preventDefault();
                        if(name != "" ){
                            try {
                                let result;

                                result = await axios.post("/api/publisher", {
                                    name: name,
                                });


                                console.log(result);

                                router.push("/backend/publishers");
                            }catch (err) {
                                console.log(err);
                            }
                        }
                    }}>
                        <div className="flex flex-row gap-8">
                            <div className="w-full">
                                <label htmlFor="book_name"
                                       className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Name</label>
                                <input type="text" id="book_name"
                                       className="bg-gray-50 border focus:outline-none border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white"
                                       placeholder="Name of the publisher" required
                                       onChange={(e) => setName(e.target.value)}
                                />
                            </div>
                        </div>

                        <div className="flex flex-row justify-center items-center">
                            <button type="submit"
                                    className="w-1/3 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </Sidebar>
    );
}
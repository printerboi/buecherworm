"use client";
import Sidebar from "@/components/Sidebar/Sidebar";
import moment from "moment";
import {useEffect, useRef, useState} from "react";
import axios from "axios";
import {CanvasEdge, CanvasNode} from "@/lib/util/GraphDataStructure";
import dynamic from "next/dynamic";

import {ForceGraphMethods} from "react-force-graph-2d";
import {useWindowSize} from "@react-hook/window-size";
const ForceGraph2d = dynamic(() => import("react-force-graph-2d"), {
    ssr: false
});

interface Stats {
    bookCount: number,
    unreadBooks: number,
    booksPearMonthThisYear: number,
    tagNetwork: {
        nodes: Array<CanvasNode>,
        edges: Array<CanvasEdge>,
    }
}



export default function Dashboard(){
    const [ stats, setStats ] = useState<Stats>()
    const fgRef = useRef<ForceGraphMethods>();
    const [width, height] = useWindowSize();


    useEffect(() => {

        const getData = async () => {
            const response = await axios.get("/api/stats");

            if(response.data.stats){
                const queriedStats = (response.data.stats as unknown ) as Stats;
                console.log(queriedStats);
                setStats(queriedStats);
            }
        }

        getData()
    }, []);


    return (
        <Sidebar page={"dashboard"}>
            <h1 className="mb-8 text-4xl font-bold">Statistics</h1>
            <div className="container rounded-lg h-full">
                <div className="max-w-full bg-gray-50 mx-4 py-6 sm:mx-auto sm:px-6 lg:px-8 mb-10">
                    <div className="sm:flex sm:space-x-4">
                        <div
                            className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow transform transition-all mb-4 w-full sm:w-1/3 sm:my-8">
                            <div className="bg-white p-5">
                                <div className="sm:flex sm:items-start">
                                    <div className="text-center sm:mt-0 sm:ml-2 sm:text-left">
                                        <h3 className="text-sm leading-6 font-medium text-gray-400">Total Books</h3>
                                        <p className="text-3xl font-bold text-black">{(stats) ? stats.bookCount : "Loading"}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow transform transition-all mb-4 w-full sm:w-1/3 sm:my-8">
                            <div className="bg-white p-5">
                                <div className="sm:flex sm:items-start">
                                    <div className="text-center sm:mt-0 sm:ml-2 sm:text-left">
                                        <h3 className="text-sm leading-6 font-medium text-gray-400">Unread Books</h3>
                                        <p className="text-3xl font-bold text-black">{(stats) ? stats.unreadBooks : "Loading"}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow transform transition-all mb-4 w-full sm:w-1/3 sm:my-8">
                            <div className="bg-white p-5">
                                <div className="sm:flex sm:items-start">
                                    <div className="text-center sm:mt-0 sm:ml-2 sm:text-left">
                                        <h3 className="text-sm leading-6 font-medium text-gray-400">Avg. Books per
                                            month {moment().format("Y")}</h3>
                                        <p className="text-3xl font-bold text-black">{(stats) ? stats.booksPearMonthThisYear.toFixed(2) : "Loading"}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="bg-white mt-10">
                    <h1 className="mb-4 text-2xl font-bold">Tag network</h1>
                </div>

                <div className="container w-full h-1/2 fixed rounded-3xl">
                    {(stats)?
                        <ForceGraph2d
                            graphData={{
                                nodes: stats.tagNetwork.nodes,
                                links: stats.tagNetwork.edges,
                            }}
                            nodeAutoColorBy="group"
                            width={width}
                            height={500}
                            backgroundColor={"rgb(249, 250, 251)"}
                            nodeVal={"size"}
                            nodeLabel={"label"}
                            enableZoomInteraction={true}
                            nodeColor={"color"}
                        />: <></>
                    }
                </div>
            </div>
        </Sidebar>
    );
}
#!/bin/sh
# This script checks if the container is started for the first time.

datbase_user=$BUECHERWORM_POSTGRES_USER
database_password=$BUECHERWORM_POSTGRES_PASSWORD

export DATABASE_URL="postgresql://${BUECHERWORM_POSTGRES_USER}:${BUECHERWORM_POSTGRES_PASSWORD}@${BUECHERWORM_POSTGRES_HOST}:${BUECHERWORM_POSTGRES_PORT}/${BUECHERWORM_POSTGRES_USER}?schema=public"
datbaseurl=DATABASE_URL

CONTAINER_FIRST_STARTUP="CONTAINER_FIRST_STARTUP"
if [ ! -e /$CONTAINER_FIRST_STARTUP ]; then
    touch /$CONTAINER_FIRST_STARTUP
    # place your script that you only want to run on first startup.

    user=$BUECHERWORM_USER
    rawPassword=$BUECHERWORM_PASSWORD
    pepper=$BUECHERWORM_PEPPER
    salt=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13; echo)

    constructed=$rawPassword$salt$pepper

    echo "Generating password..."
    hash=$(htpasswd -nbBC 12 dummy $constructed | sed 's/dummy://' | tr -d '\n' );


    echo "Installing Prisma..."
    npm install @prisma/client
    npx prisma generate
    npx prisma db push

    sessionpwd=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 32; echo)

    echo "Initial settings."
    #echo "TRUNCATE TABLE \"User\"" | npx prisma db execute --stdin --schema prisma/schema.prisma
    #echo "INSERT INTO \"User\" (username, password, salt) VALUES ('$user', '$hash', '$salt');" | npx prisma db execute --stdin --schema prisma/schema.prisma
    #echo "INSERT INTO \"Settings\" (name, value) VALUES ('established', '1');" | npx prisma db execute --stdin --schema prisma/schema.prisma
    echo "INSERT INTO \"Settings\" (name, value) VALUES ('sessionpwd', '${sessionpwd}');" | npx prisma db execute --stdin --schema prisma/schema.prisma

fi

HOSTNAME="0.0.0.0" node server.js